
# Title : DNA extraction of ylic133: ura3- ade2- clones anad PCR

# Date
Wednesday 17072019

# Objective

To extract the DNA to precisely check that neither ade3 nor ura3 is in the genome.

# Method
- DNA extraction KIT EurX
- Elution of clones 1-3: 50ul because they were dense after overnight culturing.
- Elution of clones 4-5:10ul because they were not dense at all after overnight culturing.
- DNA nanodrop
   > ylic133_1: 28.9 ng/ul

   > ylic133_2: 32.8 ng/ul

   > ylic133_3: 16.9 ng/ul

   > ylic133_4: 2.7 ng/ul (This one I had a very small pellet at the starting)

   > ylic133_5: 6.5 ng/ul (This one I had a very small pellet at the starting)

- PCR with primer 22 and primer 23

# Results :)
- DNA Gel 😁😁 The bands are ALL IN THE RIGHT LENGTH
![](../images/ylic133_pcr_biolog-replic-2019-07-17-17hr-43min-edited.png)

# Conclusion
- These clones are not pink in the re-streaking plate in YPD after 2 days of incubation.. so if they dont have the right deletion I should take the other ones 6, 7 and  9 which are pink.
- I will do also the genomic prep for the pink clones from the 5FOA plates, namely , clones, 6,7 and 9.
