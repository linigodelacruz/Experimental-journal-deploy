|   | 1  |  2 |  3 | 4  | 5  |  6 | 7  | 8  | 9  |
|:---:|---|---|---|---|---|---|---|---|---|
| A  | bacteria containing <br> plasmid  pBK49   |   |   |   |   | ylic132_5 | ylic132_5 | ylic128 | yLL135 |
|  B | bacteria containing <br> plasmid  pBK49  |   |   |   |   |   |   | ylic128 | yLL135 |
| C  | bacteria containing <br> plasmid  pBK49  |   |   |   |   |   |   | ylic130  | yLL135  |
| D  | bacteria containing <br> plasmid  pBK49  |   |   |   |   |   |   | ylic130 | yLL135  |
| E  | ByK832 |   |   |   |   |   |   | ylic131  | ylic131  |
|  F | ByK832 |   |   |   |   |   |   | ylic132_1 | ylic132_1 |
| G  | ByK352 |   |   |   |   |   |   | ylic132_2 | ylic132_2 |
|  H | ByK352  |   |   |   |   |   |   | ylic132_3 | ylic132_3 |
|  I |   |   |   |   |   |   |   | ylic132_4 | ylic132_4 |
